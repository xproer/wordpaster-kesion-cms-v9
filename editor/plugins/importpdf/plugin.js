﻿CKEDITOR.plugins.add('importpdf',
{
	init: function(editor)
	{
		editor.addCommand('importpdf',
		{
			exec: function(editor)
            {
                window.zyOffice.SetEditor(editor).api.openPdf();
			}
		});
		editor.ui.addButton('importpdf',
		{
			label: '导入PDF文档',
			command: 'importpdf',
			icon: this.path + 'images/z.png'
		});
	}
});
