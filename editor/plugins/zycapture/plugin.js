﻿CKEDITOR.plugins.add('zycapture',
{
	init: function(editor)
	{
		editor.addCommand('zycapture',
		{
			exec: function(editor)
            {
                window.zyCapture.setEditor(editor).Capture2();
			}
		});
		editor.ui.addButton('zycapture',
		{
			label: '截屏（zyCapture）',
			command: 'zycapture',
			icon: this.path + 'images/z.png'
		});
	}
});
