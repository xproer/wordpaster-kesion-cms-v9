﻿CKEDITOR.plugins.add('pptimport',
{
	init: function(editor)
	{
		editor.addCommand('pptimport',
		{
			exec: function(editor)
            {
                WordPaster.getInstance().SetEditor(editor);
                WordPaster.getInstance().importPPT();
			}
		});
		editor.ui.addButton('pptimport',
		{
            label: 'PowerPoint一键导入',
			command: 'pptimport',
			icon: this.path + 'images/ppt.png'
		});
	}
});
